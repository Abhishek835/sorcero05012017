<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="container text-center">
        <div class="row content-center">
            <div class="col-md-12 col-lg-12" style="text-align: left;">
            <div class="header header-primary text-center">
                    <div style="padding-bottom: 10%;">
                        <img style="width:10%; float: left;" src="<?php echo base_url(); ?>assets/img/new_logo-1.png" alt=""><br />
                    </div>
                </div>
                <h2 class="title">Learning Platform</h2>
                <h5 class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</h5>
				<a class="btn btn-primary btn-simple " href="<?php echo base_url('index.php/'); ?>users/login">Login</a>
				<a class="btn btn-primary btn-simple " href="<?php echo base_url('index.php/'); ?>users/registration">Create an Account</a>
            </div>
        </div>
    </div>
