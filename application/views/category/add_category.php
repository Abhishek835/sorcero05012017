<div class="container">
    <div class="col-md-4 content-center">
        <div class="card card-login card-plain">
            <form class="form" method="POST" action="<?php echo base_url('index.php/'); ?>categorys/add_category">
                
                <div class="content">
                    <?php 
                    if(count($edit_category)>0){
                   // print_r($edit_category); 
                    } ?>
                    <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons users_circle-08"></i>
                        </span>
                        <input type="text" class="form-control" name="name" placeholder="Name" required="" value="<?php echo !empty($edit_category['name'])?$edit_category['name']:''; ?>">
                      
                    </div>
					 <p> <?php echo form_error('name','<span class="help-block">','</span>'); ?></p>
                   


             <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="fa fa-dollar"></i>
                        </span>
                        <input type="text" class="form-control" name="price" placeholder="Price"  value="<?php echo !empty($edit_category['price'])?$edit_category['price']:''; ?>">
                      
                    </div>
                    

             <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="fa  fa-th-large"></i>
                        </span>
                        <?php
                    echo '<select class="form-control" name="parent" >';
                    echo '<option value="">Select parent</option>';
                        foreach($this->category->categories() as $category):  ?>
                            <option value="<?php echo $category->id;?>" <?php  if(!empty($edit_category['parent'])){  if($category->id==$edit_category['parent']){ echo 'selected';} }?>><?php echo $category->name;?></option>

                        <?php endforeach;
                    echo '</select>';
                      ?>
                       
                    </div>
                    
                </div>

            <input type="hidden" name="edit_id" value="<?php echo !empty($edit_category['id'])?$edit_category['id']:'0'; ?>">

                <div class="footer text-center">
                    <input type="submit" name="regisSubmit" class="btn btn-primary btn-lg btn-block" value="Submit"/>
                </div>
              
            </form>
        </div>
    </div>
</div>

