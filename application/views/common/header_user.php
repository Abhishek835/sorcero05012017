<?php
        if(!$this->session->userdata('isUserLoggedIn')){
            redirect('users/login');
        }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Sorcero</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?php echo base_url(); ?>assets/css/demo.css" rel="stylesheet" />
</head>

<style type="text/css">



.page-header .container {
    overflow: visible;
}


.form-control > option{
 
color: dimgray;
}

.succc{
    color: green;
    font-size: 12px;
    font-weight: 700;

}
.errors{
    color: red;
    font-size: 12px;
    font-weight: 700;
    
}

a {
    
    text-decoration: none !important;
}

.cursor_not{
        cursor: not-allowed;
}
th{
    text-align: center;
}
table.dataTable tbody tr {
     background-color: transparent !important;
}

 .paginate_button, #examplesdfsf_info, label{
    color: white;
    font-weight: bold;
}
.page-header .container>.content-center {
    top: 70%;

}
select.form-control {
        padding: 0 !important;
}
body{
        background: #26140A;
}


.page-header .container>.content-center {
width: 33%;

}

.col-ms-4{
    width: 33%;
}

.col-ms-8{
    width: 66%;
}

.col-sm-1{
    float: left;
}

</style>

<body class="login-page sidebar-collapse">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
        <div class="container">
            <div class="dropdown button-dropdown">
                <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-header">Dropdown header</a>
                    <a class="dropdown-item" href="<?php echo base_url('index.php/'); ?>users/dashboard">Dashboard</a>
                    <a class="dropdown-item" href="<?php echo base_url('index.php/'); ?>users/list">Users</a>

                    <a class="dropdown-item" href="<?php echo base_url('index.php/'); ?>users/account">Account</a>
                    <a class="dropdown-item" href="<?php echo base_url('index.php/'); ?>users/logout">Logout</a>

                </div>
            </div>
            <div class="navbar-translate">
                <a class="navbar-brand" href="<?php echo base_url('index.php/'); ?>users/account" rel="tooltip" title="" data-placement="bottom" target="_blank">
                    <p>Welcome <?php echo $user['name']; ?>!</p>
                </a>
                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="<?php echo base_url(); ?>assets/img/blurred-image-1.jpg">
                <ul class="navbar-nav">
<!--
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('index.php/'); ?>users/account"><p>Welcome <?php echo $user['name']; ?>!</p></a>
                    </li>
-->
                    
                </ul>
            </div>
        </div>
    </nav>
    <div class="page-header" filter-color="orange">
        <div class="page-header-image" style="background-image:url(<?php echo base_url(); ?>assets/img/login.jpg)"></div>