<div class="container">
    <div class="col-md-4 content-center">
        <div class="card card-login card-plain">
            <form class="form" method="POST" action="<?php echo base_url('index.php/'); ?>users/forgot_password">
                <div class="header header-primary text-center">
                    <div style="padding-bottom: 10%;">
                        <img src="<?php echo base_url(); ?>assets/img/now-logo.png" alt="">
                    </div>
                </div>
                <div class="content">
                    <?php
                    if(!empty($success_msg)){
                        ?>
                        <div class="alert alert-success" role="alert">
                            <div class="container">
                                <div class="alert-icon">
                                    <i class="now-ui-icons ui-2_like"></i>
                                </div>
                                <strong>Well done!</strong> <?php echo $success_msg; ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">
                                        <i class="now-ui-icons ui-1_simple-remove"></i>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <?php }elseif(!empty($error_msg)){ ?>
                        <div class="alert alert-danger" role="alert">
                            <div class="container">
                                <div class="alert-icon">
                                    <i class="now-ui-icons objects_support-17"></i>
                                </div>
                                <strong>Oh snap!</strong> <?php echo $error_msg; ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">
                                        <i class="now-ui-icons ui-1_simple-remove"></i>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <?php } ?>
                        
                    <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons users_circle-08"></i>
                        </span>
                        <input type="email" name="email" class="form-control" placeholder="e.g. example@demo.com">
						
                      
                    </div>
                     <p style="color:red;"> <?php echo form_error('email','<span class="help-block">','</span>'); ?></p>
                </div>
                <div class="footer text-center">
                    <input type="submit" name="forgot" class="btn btn-primary btn-lg btn-block" value="Submit"/>
                </div>
                
				
            </form>
        </div>
    </div>
</div>
