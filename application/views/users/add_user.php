<div class="container">
    <div class="col-md-4 content-center">
        <div class="card card-login card-plain">
            <form class="form" method="POST" action="<?php echo base_url('index.php/'); ?>users/add_user">
                <div class="header header-primary text-center">
                    <div style="padding-bottom: 10%;">
                        <img src="<?php echo base_url(); ?>assets/img/now-logo.png" alt="">
                    </div>
                </div>
                <div class="content">

  <?php
                    if(!empty($success_msg)){
                        ?>
                        <div class="alert alert-success" role="alert">
                            <div class="container">
                                <div class="alert-icon">
                                    <i class="now-ui-icons ui-2_like"></i>
                                </div>
                                <strong>Well done!</strong> <?php echo $success_msg; ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">
                                        <i class="now-ui-icons ui-1_simple-remove"></i>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <?php }elseif(!empty($error_msg)){ ?>
                        <div class="alert alert-danger" role="alert">
                            <div class="container">
                                <div class="alert-icon">
                                    <i class="now-ui-icons objects_support-17"></i>
                                </div>
                                <strong>Oh snap!</strong> <?php echo $error_msg; ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">
                                        <i class="now-ui-icons ui-1_simple-remove"></i>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <?php } ?>
                    


                    <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons users_circle-08"></i>
                        </span>
                        <input type="text" class="form-control" name="name" placeholder="Name" required="" value="">
                      
                    </div>
               <p> <?php echo form_error('name','<span class="help-block">','</span>'); ?></p>


                    <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons ui-1_email-85"></i>
                        </span>
                        <input type="email" class="form-control" name="email" placeholder="Email" required="" value="">
                     
                    </div>

					 <p>  <?php echo form_error('email','<span class="help-block">','</span>'); ?></p>



              <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons users_circle-08"></i>
                        </span>
                        <input type="text" class="form-control" name="username" placeholder="User name" required="" value="">
                      
                    </div>
               <p> <?php echo form_error('username','<span class="help-block">','</span>'); ?></p>



                    <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons objects_globe"></i>
                        </span>
                        <input type="text" class="form-control" name="site_url" placeholder="siteaddress" value="">    
                    </div>
                    <p style="text-align: left;">e.g. https://<strong>siteaddress</strong>.sorcero.com</p>



                    <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons objects_key-25"></i>
                        </span>
                        <input type="password" class="form-control" name="password" placeholder="Password" required="">
                       
                    </div>


					 <p><?php echo form_error('password','<span class="help-block">','</span>'); ?></p>


                    <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons objects_key-25"></i>
                        </span>
                        <input type="password" class="form-control" name="conf_password" placeholder="Confirm password" required="">
                       
                    </div>
					<p> <?php echo form_error('conf_password','<span class="help-block">','</span>'); ?></p>



  <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons fa fa-sticky-note-o"></i>
                        </span>
                      <!--   <input type="password" class="form-control" name="password" placeholder="Password" required=""> -->
                      <textarea name="bio" class="form-control"  placeholder="Biography" required> </textarea>
                       
                    </div>


                     <p><?php echo form_error('bio','<span class="help-block">','</span>'); ?></p>



            <div class="input-group form-group-no-border input-lg">
                        <span class="input-group-addon">
                            <i class="now-ui-icons fa fa-user"></i>
                        </span>
                 
                     <?php
                    echo '<select class="form-control" name="role" required="">';
                        foreach($this->user->role_list() as $role_list_value):
                    ?>
                   <option value="<?php echo $role_list_value->id;?>" ><?php echo $role_list_value->role;?></option>

                      <?php   endforeach;
                    echo '</select>';
                      ?>
                       
                    </div>
        <p><?php echo form_error('roll','<span class="help-block">','</span>'); ?></p>

                </div>






                <div class="footer text-center">
                    <input type="submit" name="regisSubmit" class="btn btn-primary btn-lg btn-block" value="Submit"/>
                </div>
                <div class="pull-left">
                    <h6><a href="<?php echo base_url('index.php/'); ?>users/login" class="link">Login</a></h6>
                </div>
            </form>
        </div>
    </div>
</div>
