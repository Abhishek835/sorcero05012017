<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User Management class created by parthujeniya@gmail.com
 */
class Courses extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('course');
         $this->load->model('user');
         $this->load->model('category');
    }
    
    /*
     * User account information
     */



    /*
     * add ategory
     */
    public function create(){
        $data = array();

  $uid =  $this->uri->segment(3);
    if($uid!=""){
         $data['edit_course'] = $this->course->all_courses($uid );
     }else{
         $data['edit_course'] = array();
     }
 

     if(!$this->session->userdata('isUserLoggedIn')){
           
            redirect('users/login');
        }


if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }


        $userData = array();
        if($this->input->post('regisSubmit'))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('category', 'Category', 'required');
            $this->form_validation->set_rules('price', 'price', 'required');
            $this->form_validation->set_rules('days', 'days', 'required');
            $this->form_validation->set_rules('level', 'level', 'required');
            
       


        

           


                $config['upload_path']          = './images/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 221000;
                $config['max_width']            = 111024;
                $config['max_height']           = 7681;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
                     $this->upload->display_errors();
              
                     
                     if(!empty($data['edit_course']['image'])){
                  $image = $data['edit_course']['image']; 
                     }else{
                    $image = 'sorcero_logo.png
                    ';
                     }
                 
              
                       
                }
                else
                {
                    $data =  $this->upload->data();
                       // var_dump($data);
                $image =  $data['file_name'];
                    
                }

                echo $image;

         $categoryData = array(
                'name' => strip_tags($this->input->post('name')),
                'category' => strip_tags($this->input->post('category')),
                'course_code' => $this->input->post('course_code'),

                'price' => strip_tags($this->input->post('price')),
                'description' => strip_tags($this->input->post('description')),
                'days' => $this->input->post('days'),
                 'level' => $this->input->post('level'),
                 'image' => $image,
              
            );

          

           $edit_id = $this->input->post('edit_id');



            if($this->form_validation->run() == true){
                if($edit_id!=0){
             $insert =   $this->db->where('id', $edit_id);
            $this->db->update('courses', $categoryData); 


             $msg = 'Your courses was successfully updated.';

                }else{
                  $insert = $this->course->insert($categoryData);  
                       $msg = 'Your courses was successfully added.';
                }
                
                if($insert){
                    $this->session->set_userdata('success_msg', $msg);
                    redirect('courses/course');
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }


    $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));




  


          $sdfd = $this->category->categories();

      
        //load the view
         $this->load->view('common/header_user', $data);
        $this->load->view('courses/create', $data);
        $this->load->view('common/footer', $data);
    }
    




    /*
     * all  course
     */
    public function course(){
        $data = array();



     if(!$this->session->userdata('isUserLoggedIn')){
           
            redirect('users/login');
        }

        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }



          $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));


          $data['course'] = $this->course->courses();


        //load the view
        $this->load->view('common/header_user', $data);
        $this->load->view('courses/courses', $data);
        $this->load->view('common/footer', $data);
    }
    





  public function delete_courses(){


    $id =  strip_tags($this->input->post('id'));


    $delete =  $this->db->where('id', $id);
      $this->db->delete('courses'); 

       if($delete){
                   echo '&nbsp;<br><span class="succc">Deleted</span>';
                }else{
                   echo '&nbsp;<br><span class="errors">Error</span>';
                }



  }


  

  public function selectrt(){



    $id =  strip_tags($this->input->post('id'));

    $course =  strip_tags($this->input->post('course'));
    $type =  strip_tags($this->input->post('type'));

   
         $coursedata = $this->course->all_courses($course);
//print_r($coursedata);

//echo $id.$course;




  if($type=='rem'){
      $gh= 1;

            $sdgsf = explode(",", $coursedata['users']);
              foreach ($sdgsf as $key => $value) {
                   if($value=$id){
                    unset($sdgsf[$key]);
                   }
              }
            $new = implode(",", $sdgsf);

            $updateData  = array(
            'users'=> $new 
                );
              $insert =   $this->db->where('id', $course);
                        $this->db->update('courses', $updateData); 
              


  }else{  

                 $gh= 0;

                if($coursedata['users']!=''){

                 $new = $coursedata['users'].",".$id;   
                }else{
                    $new = $id; 
                }

                   
                $updateData  = array(
                'users'=> $new 
                    );
                  $insert =   $this->db->where('id', $course);
                            $this->db->update('courses', $updateData); 
  }







       if($gh==1){ ?>
       <button class="btn btn-default" style="padding:1px" onclick="selectss('<?php echo $id;?>','<?php echo $course;?>','sel')">Select</button>
                <?php }else{ ?>
          <button class="btn btn-success" style="padding:1px" onclick="selectss('<?php echo $id;?>','<?php echo $course;?>','rem')">Remove</button>
                <?php } 


  }







  /*
     * see course
     */
    public function courseSee(){
        $data = array();


$uid =  $this->uri->segment(3);
  $data['coursedata'] = $this->course->all_courses($uid);


     if(!$this->session->userdata('isUserLoggedIn')){
           
            redirect('users/login');
        }

        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }



          $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));


          $data['course'] = $this->course->courses();

      
      //load the view
         $this->load->view('common/header_user', $data);
        $this->load->view('courses/see_course', $data);
        $this->load->view('common/footer', $data);
    }
    

/*add Content*/

public function add_content(){
 $id =  strip_tags($this->input->post('id'));
$form = '<form method="post" name="" id="addcontent">



   <div class="input-group form-group-no-border input-lg">
                <span class="input-group-addon">
                <i class="fa  fa-clock-o"></i>
          </span>
        <input type="text" class="form-control" name="Name" placeholder="Name"   value="">
                        
         </div>
    <div class="input-group form-group-no-border input-lg">
        <span class="input-group-addon">
           <i class="fa  fa-quote-right"></i>
       </span>
       <textarea name="description" class="form-control" placeholder="Description"></textarea>
       </div>

 <div class="footer text-center">
                    <input type="submit" name="regisSubmit" class="btn btn-primary btn-lg btn-block" value="Submit"/>
                </div>
</form>';


return $form;

}

    





}

