<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User Management class created by parthujeniya@gmail.com
 */
class Categorys extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('category');
         $this->load->model('user');
    }
    
    /*
     * User account information
     */



    /*
     * add ategory
     */
    public function add_category(){
        $data = array();

     if(!$this->session->userdata('isUserLoggedIn')){
           
            redirect('users/login');
        }


if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }


        $userData = array();
        if($this->input->post('regisSubmit'))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');
            
       


        

            $categoryData = array(
                'name' => strip_tags($this->input->post('name')),
                'parent' => strip_tags($this->input->post('parent')),
                'price' => $this->input->post('price'),
              
            );
          

           $edit_id = $this->input->post('edit_id');



            if($this->form_validation->run() == true){
                if($edit_id!=0){
             $insert =   $this->db->where('id', $edit_id);
            $this->db->update('category', $categoryData); 


             $msg = 'Your category was successfully updated.';

                }else{
                  $insert = $this->category->insert($categoryData);  
                       $msg = 'Your category was successfully added.';
                }
                
                if($insert){
                    $this->session->set_userdata('success_msg', $msg);
                    redirect('categorys/category');
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }


    $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));




    $uid =  $this->uri->segment(3);
    if($uid!=""){
         $data['edit_category'] = $this->category->all_category($uid );
     }else{
         $data['edit_category'] = array();
     }
 



          $sdfd = $this->category->categories();

      
        //load the view
         $this->load->view('common/header_user', $data);
        $this->load->view('category/add_category', $data);
        $this->load->view('common/footer', $data);
    }
    




    /*
     * all category
     */
    public function category(){
        $data = array();



     if(!$this->session->userdata('isUserLoggedIn')){
           
            redirect('users/login');
        }

        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }



          $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));


          $sdfd = $this->category->categories();

      




        //load the view
         $this->load->view('common/header_user', $data);
        $this->load->view('category/category', $data);
        $this->load->view('common/footer', $data);
    }
    


  public function delete_category(){


    $id =  strip_tags($this->input->post('id'));


    $delete =  $this->db->where('id', $id);
      $this->db->delete('category'); 

       if($delete){
                   echo '&nbsp;<br><span class="succc">Deleted</span>';
                }else{
                   echo '&nbsp;<br><span class="errors">Error</span>';
                }



  }








	


}