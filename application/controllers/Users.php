<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User Management class created by parthujeniya@gmail.com
 */
class Users extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('user');
    }
    
    /*
     * User account information
     */



    public function account(){
        $data = array();
        if($this->session->userdata('isUserLoggedIn')){
            $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));
            //load the view
            $this->load->view('common/header_user', $data);
            $this->load->view('users/account', $data);
            $this->load->view('common/footer_user', $data);

        }else{
            redirect('users/login');
        }
    }

    public function dashboard(){
        $data = array();
        if($this->session->userdata('isUserLoggedIn')){
            $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));
            //load the view
            $this->load->view('common/header_user', $data);
            $this->load->view('users/dashboard', $data);
            $this->load->view('common/footer_user', $data);

        }else{
            redirect('users/login');
        }
    }

    
    /*
     * User login
     */
    public function login(){
        $data = array();
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }


        if($this->input->post('loginSubmit')){
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_verify_check');

            if ($this->form_validation->run() == true) {
                $con['returnType'] = 'single';
                $con['conditions'] = array(
                    'email'=>$this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'status' => '1'
                );
                $checkLogin = $this->user->getRows($con);
                if($checkLogin){
                    $this->session->set_userdata('isUserLoggedIn',TRUE);
                    $this->session->set_userdata('userId',$checkLogin['id']);
                    redirect('users/account/');
                }else{
                    $data['error_msg'] = 'Wrong email or password, please try again.';
                }
            }
        }


        //load the view
        $this->load->view('common/header', $data);
        $this->load->view('users/login', $data);
        $this->load->view('common/footer', $data);
    }
    
    /*
     * User registration
     */
    public function registration(){
        $data = array();
        $userData = array();
        if($this->input->post('regisSubmit'))
		{
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
            $this->form_validation->set_rules('site_url', 'Site Url', 'required|callback_siteurl_check');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('conf_password', 'confirm password', 'required|matches[password]');
            $this->form_validation->set_rules('username', 'Username', 'required');

       


        

            $userData = array(
                'name' => strip_tags($this->input->post('name')),
                'email' => strip_tags($this->input->post('email')),
                'password' => md5($this->input->post('password')),
                'username' => strip_tags($this->input->post('username')),
                'site_url' => strip_tags($this->input->post('site_url'))
            );

            if($this->form_validation->run() == true){
                $insert = $this->user->insert($userData);
                if($insert){
                    $this->session->set_userdata('success_msg', 'Your registration was successfully. Please login to your account.');
                    redirect('users/login');
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }
        $data['user'] = $userData;
        //load the view
        $this->load->view('common/header', $data);
        $this->load->view('users/registration', $data);
        $this->load->view('common/footer', $data);
    }
    




    /*
     * User logout
     */
    public function logout(){
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('userId');
        $this->session->sess_destroy();
        redirect('users/login/');
    }
    
    /*
     * Existing email check during validation
     */
    public function email_check($str){
        $con['returnType'] = 'count';
        $con['conditions'] = array('email'=>$str);
        $checkEmail = $this->user->getRows($con);
        if($checkEmail > 0){
            $this->form_validation->set_message('email_check', 'The given email already exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }


    public function siteurl_check($str){
        $con['returnType'] = 'count';
        $con['conditions'] = array('site_url'=>$str);
        $checkEmail = $this->user->getRows($con);
        if($checkEmail > 0){
            $this->form_validation->set_message('siteurl_check', 'The given Site URL already exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }


    public function email_verify_check($str){
        $con['returnType'] = 'count';
        $con['conditions'] = array('email'=>$str,'email_verify'=>'1');
        $checkEmail = $this->user->getRows($con);
        if($checkEmail > 0){
            return TRUE;
        } else {
            $this->form_validation->set_message('email_verify_check', 'The given email is not verify');
            return FALSE;
        }
    }

    public function email_verify(){

$url_data = $this->uri->uri_to_assoc();
$str = $url_data[0];
print_r($url_data);
$data = array('email_verify' => 1);
$where = "id = '$str'";
$str=$this->db->update_string('users', $data, $where);
$this->db->query($str);

redirect('users/login');

    }
	
	
	
	/*
     * User forgot-password
     */
    public function forgot_password()
	{

		$data = array();
		if($this->input->post('forgot'))
		{
			//echo "asdasdasd";
			
			
			$str = strip_tags($this->input->post('email'));
			$pass=rand(9999,999999);
			$data = array('password' =>md5($pass));
			$where = "email = '$str'";
			$str1=$this->db->update_string('users', $data, $where);
			$forget=$this->db->query($str1);

			if($forget)
			{
				$message = "Password :- ".$pass;
				$this->load->library('email');
				$this->email->set_header('MIME-Version', '1.0; charset=utf-8');
				//$this->email->set_header('Content-type', 'text/html');
				$this->email->from('support@sorcero.net', 'SORCERO');
				$this->email->to($str);
				//$this->email->cc('jitu.maestros@gmail.com');
				//$this->email->bcc('atul.maestros@gmail.com');
				$this->email->subject('Forgot Password');
				$this->email->message($message);
				//$this->email->set_alt_message('This is the alternative message');
				//$this->email->attach('https://www.sorcero.net/assets/img/now-logo.png');
				//$this->email->attach('http://beta.capitaltravel.com/img/logo.png');
				$mail= $this->email->send();
				if($mail)
				{
					$data['success_msg']='You have a mail on your registered email check and use the password.';
					//redirect('users/login');
				}else{
					
					  $data['error_msg'] = 'Wrong email or password, please try again.';
				}
			
			}
			
			
		}   
        //$data['user'] = $userData; 
        //load the view
        $this->load->view('common/header', $data);
        $this->load->view('users/forgot-password', $data);
        $this->load->view('common/footer', $data);
		//echo "xczc"; 
    }
	
	

    public function list(){
        $data = array();
        if($this->session->userdata('isUserLoggedIn')){
            $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));
            //load the view
            $this->load->view('common/header_user', $data);
            $this->load->view('users/list', $data);
            $this->load->view('common/footer_user', $data);

        }else{
            redirect('users/login');
        }
    }
	

    public function create(){
        $data = array();
        $userData = array();
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }
        if($this->input->post('regisSubmit'))
		{

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
           // $this->form_validation->set_rules('site_url', 'Site Url', 'required|callback_siteurl_check');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('conf_password', 'confirm password', 'required|matches[password]');
             $this->form_validation->set_rules('bio', 'bio', 'required');
              $this->form_validation->set_rules('username', 'username', 'required');



         $config['upload_path']          = './images/';
                $config['allowed_types']        = 'gif|jpg|png';
                 $config['max_size']             = 221000;
                    $config['max_width']            = 111024;
                $config['max_height']           = 7681;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
                     echo $this->upload->display_errors();
                        die;
                }
                else
                {
                    $data =  $this->upload->data();
                       // var_dump($data);
                $image =  $data['file_name'];
                        
                }


            $userData = array(
                'name' => strip_tags($this->input->post('name')),
                'email' => strip_tags($this->input->post('email')),
                'role' => strip_tags($this->input->post('role')),
                'password' => md5($this->input->post('password')),
                'site_url' => strip_tags($this->input->post('site_url')),
                'bio' => $this->input->post('bio'),
                'username' => strip_tags($this->input->post('username')),
                'image' => $image
            );

            if($this->form_validation->run() == true){
                $insert = $this->user->insert($userData);
                if($insert){
                    $this->session->set_userdata('success_msg', 'You created a user.');
                    redirect('users/create');
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }
       // $data['user'] = $userData;


        if($this->session->userdata('isUserLoggedIn')){
            $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));
            //load the view
            $this->load->view('common/header_user', $data);
            $this->load->view('users/create', $data);
            $this->load->view('common/footer_user', $data);

        }else{
            redirect('users/login');
        }
        //load the view

    }





  public function edit_user(){

   //$this->load->library('uri');

$uid =  $this->uri->segment(3);
   $data['course'] = $this->course->all_courses($uid );


        $userData = array();
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }



   if($this->session->userdata('isUserLoggedIn')){
       
$data['user'] = $this->user->getRows(array('id'=>$uid));

 //print_r($data);
            //load the view
            $this->load->view('common/header_user', $data);
            $this->load->view('users/see_course', $data);
            $this->load->view('common/footer_user', $data);

        }else{
            redirect('users/login');
        }


  }






  public function delete_user(){


    $id =  strip_tags($this->input->post('id'));


    $delete =  $this->db->where('id', $id);
      $this->db->delete('users'); 

    if($delete){
                   echo '&nbsp;<br><span class="succc">Deleted</span>';
                }else{
                   echo '&nbsp;<br><span class="errors">Error</span>';
                }

  }






//rolls  add edit



    public function add_role(){
        $data = array();

     if(!$this->session->userdata('isUserLoggedIn')){
           
            redirect('users/login');
        }


if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }


        $userData = array();
        if($this->input->post('regisSubmit'))
        {
            $this->form_validation->set_rules('role', 'User type', 'required');
            
       


        

            $categoryData = array(
                'role' => strip_tags($this->input->post('role')),
                
              
            );
          

           $edit_id = $this->input->post('edit_id');



            if($this->form_validation->run() == true){
                if($edit_id!=0){
             $insert =   $this->db->where('id', $edit_id);
            $this->db->update('role', $categoryData); 


             $msg = 'Your role was successfully updated.';

                }else{
               //   $insert = $this->category->insert($categoryData);  
                   $insert = $this->db->insert('role', $categoryData);
                       $msg = 'Your role was successfully added.';
                }
                
                if($insert){
                    $this->session->set_userdata('success_msg', $msg);
                    redirect('users/role');
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }


    $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));




    $uid =  $this->uri->segment(3);
    if($uid!=""){
         $data['edit_role'] = $this->user->role_list($uid );
     }else{
         $data['edit_role'] = array();
     }
 
      
        //load the view
         $this->load->view('common/header_user', $data);
        $this->load->view('users/add_role', $data);
        $this->load->view('common/footer', $data);
    }
    







    /*
     * all roles
     */
    public function role(){
        $data = array();



     if(!$this->session->userdata('isUserLoggedIn')){
           
            redirect('users/login');
        }

        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }



          $data['user'] = $this->user->getRows(array('id'=>$this->session->userdata('userId')));


          $sdfd = $this->user->role_list();

      
         $data['role'] =  $pt = $this->db ->get('role')->result();



        //load the view
         $this->load->view('common/header_user', $data);
        $this->load->view('users/role', $data);
        $this->load->view('common/footer', $data);
    }
   

	



 public function delete_role(){


    $id =  strip_tags($this->input->post('id'));


    $delete =  $this->db->where('id', $id);
      $this->db->delete('role'); 

    if($delete){
                   echo '&nbsp;<br><span class="succc">Deleted</span>';
                }else{
                   echo '&nbsp;<br><span class="errors">Error</span>';
                }


  }


}