<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Model{
    function __construct() {
        $this->userTbl = 'users';
    }
    /*
     * get rows from the users table
     */
    function getRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->userTbl);
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        if(array_key_exists("id",$params)){
            $this->db->where('id',$params['id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        //return fetched data
        return $result;
    }
    
    /*
     * Insert user information
     */
    public function insert($data = array()) {
        //add created and modified data if not included
        if(!array_key_exists("created", $data)){
            $data['created'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("modified", $data)){
            $data['modified'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("role", $data)){
            $data['role'] = 2;
        }
        
        //insert user data to users table
        $insert = $this->db->insert($this->userTbl, $data);
        
        //return the status
        if($insert){
            $insert_id=$this->db->insert_id();
            
            $this->email_verify_mail(Array('id'=>$insert_id));
            return $insert_id;
        }else{
            return false;
        }
    }


    public function email_verify_mail($data = array()){

$data['user'] = $this->user->getRows(array('id'=>$data['id']));

$message = 'https://www.sorcero.net/index.php/users/email_verify/0/'.$data['id'];

$this->load->library('email');
$this->email->set_header('MIME-Version', '1.0; charset=utf-8');
//$this->email->set_header('Content-type', 'text/html');
$this->email->from('support@sorcero.net', 'SORCERO');
$this->email->to($data['user']['email']);
//$this->email->cc('jitu.maestros@gmail.com');
//$this->email->bcc('atul.maestros@gmail.com');
$this->email->subject('Email Verification');
$this->email->message($message);
//$this->email->set_alt_message('This is the alternative message');
//$this->email->attach('https://www.sorcero.net/assets/img/now-logo.png');
//$this->email->attach('http://beta.capitaltravel.com/img/logo.png');
echo $this->email->send();

    }



    function role_list($params = null){


if($params!=''){

      
       $this->db->where('id',$params);
            $query = $this->db->get('role');
            $pt = $query->row_array();   



        }else{
            $pt = $this->db
        ->where('id !=', 1)->where('id !=', 2)
        ->get('role')
        ->result();
       
        }




return $pt;

    }


    function user_list($params = array()){

$pt = $this->db
//->where('time', $time)
->get('users')
->result();
return $pt;

    }



  function username($params = null){

$pt = $this->db
->where('id', $params)
->get('users')
->result();

return $pt;

    }




}