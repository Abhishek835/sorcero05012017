-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 01, 2018 at 12:58 AM
-- Server version: 5.6.36-82.1-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sorcero8_sorcero`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` int(25) NOT NULL,
  `price` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `parent`, `price`) VALUES
(1, 'category1', 0, 122),
(7, 'category2 update ', 7, 353451111),
(11, 'asdasdsad', 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `status` int(25) NOT NULL COMMENT '1=active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `users` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'sorcero_logo.png',
  `category` int(25) NOT NULL,
  `description` longtext NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `price` int(25) NOT NULL,
  `days` int(25) NOT NULL,
  `certificate` int(25) NOT NULL,
  `level` int(25) NOT NULL,
  `created` varchar(255) NOT NULL,
  `modified` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `users`, `image`, `category`, `description`, `course_code`, `price`, `days`, `certificate`, `level`, `created`, `modified`) VALUES
(2, 'Dgdfgdfg4', '1,2,27,28', 'sorcero_logo.png\n                    ', 1, 'fghfd hdfhdgdf2121', '121dd', 546, 5, 0, 17, '2017-12-22 04:07:11', '2017-12-22 04:07:11'),
(4, 'new', '28,2', 'desktop-hhc.jpg', 11, 'asf saf saf sad\r\ndsf sdf', 'cs1212', 585, 5, 0, 11, '2017-12-22 04:21:23', '2017-12-22 04:21:23');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(255) NOT NULL,
  `role` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Website Admin'),
(2, 'Super Admin'),
(3, 'Student');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `role` int(255) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'dummy_profile.png',
  `bio` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  `email_verify` int(11) NOT NULL COMMENT '1=yes, 0=no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `role`, `name`, `username`, `email`, `password`, `site_url`, `image`, `bio`, `timezone`, `created`, `modified`, `status`, `email_verify`) VALUES
(1, 0, 4, 'Abhishek sahurty', 'rytrytry', 'engg.abhisahu@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', '', '20728228_1613365448734173_2155160305898369148_n4.jpg', ' rty ryrty ', '', '2017-12-11 07:15:44', '2017-12-11 07:15:44', '1', 1),
(2, 0, 3, 'Abhishek Sahu', 'hfghfgh', 'abhimaestros@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', 'dummy_profile.png', ' ', '', '2017-12-14 23:19:54', '2017-12-14 23:19:54', '1', 1),
(27, 0, 5, 'srinivas', 'srinivas@42strategies.com', 'srinivas@42strategies.com', 'd41d8cd98f00b204e9800998ecf8427e', '', 'dummy_profile.png', 'hjghj ', '', '2017-12-21 11:14:48', '2017-12-21 11:14:48', '1', 1),
(28, 0, 2, 'name', 'user', 'bhushan.maestros@gmail.com', '0dadce173a8b6fff955f4a846b018f48', 'bhushan', 'dummy_profile.png', '', '', '2017-12-22 05:02:52', '2017-12-22 05:02:52', '1', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
